package com.bumin.reporting;

import com.bumin.reporting.model.Error;
import com.bumin.reporting.model.Status;
import com.bumin.reporting.security.AuthenticationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApplicationRestControllerAdvice {

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<Error> handleAuthenticationException(AuthenticationException e) {
       return new ResponseEntity<>(new Error(0,Status.DECLINED.toString(),e.getMessage()),
               HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
