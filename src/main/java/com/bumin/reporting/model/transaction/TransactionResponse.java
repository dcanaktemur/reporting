package com.bumin.reporting.model.transaction;

import com.bumin.reporting.model.customer.CustomerInfo;
import com.bumin.reporting.model.db.Acquirer;
import com.bumin.reporting.model.db.Merchant;

public class TransactionResponse {

    private Fx fx;
    private CustomerInfo customerInfo;
    private Merchant merchant;
    private Acquirer acquirer;
    private TransactionWrapper transaction;

    public Fx getFx() {
        return fx;
    }

    public void setFx(Fx fx) {
        this.fx = fx;
    }

    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public TransactionWrapper getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionWrapper transactionWrapper) {
        this.transaction = transactionWrapper;
    }

    public Acquirer getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(Acquirer acquirer) {
        this.acquirer = acquirer;
    }
}