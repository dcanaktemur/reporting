package com.bumin.reporting.model.transaction;


import java.util.List;

public class TransactionReportResponse {

    private String status;

    private List<TransactionReport> response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<TransactionReport> getResponse() {
        return response;
    }

    public void setResponse(List<TransactionReport> response) {
        this.response = response;
    }
}
