package com.bumin.reporting.model.db;

import com.bumin.reporting.model.customer.CustomerInfo;
import com.bumin.reporting.model.transaction.FxMerchant;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Transaction {

    private String referenceNo;
    private String status;
    private String channel;
    private String customData;
    private String chainId;
    private String operation;
    private Date updatedAt;
    private Date createdAt;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String code;
    private String message;

    @Column(unique = true)
    private String transactionId;

    @ManyToOne
    @JoinColumn(name="merchantId")
    @JsonIgnore
    private Merchant merchant;

    @ManyToOne
    @JoinColumn(name="acquirerTransactionId")
    @JsonIgnore
    private Acquirer acquirer;

    @ManyToOne
    @JoinColumn(name="agentInfoId")
    private Agent agent;

    @ManyToOne
    @JoinColumn(name="customerId")
    @JsonIgnore
    private CustomerInfo customerInfo;

    @ManyToOne
    @JoinColumn(name="fxTransactionId")
    @JsonIgnore
    private FxMerchant fxMerchant;

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCustomData() {
        return customData;
    }

    public void setCustomData(String customData) {
        this.customData = customData;
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Acquirer getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(Acquirer acquirer) {
        this.acquirer = acquirer;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }

    public FxMerchant getFxMerchant() {
        return fxMerchant;
    }

    public void setFxMerchant(FxMerchant fxMerchant) {
        this.fxMerchant = fxMerchant;
    }
}
