package com.bumin.reporting.model;

public enum Status {
    APPROVED,
    WAITING,
    DECLINED,
    ERROR;
}
