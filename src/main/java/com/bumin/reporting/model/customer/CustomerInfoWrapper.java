package com.bumin.reporting.model.customer;

public class CustomerInfoWrapper {

    private CustomerInfo customerInfo;

    public CustomerInfoWrapper(CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }

    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }
}
