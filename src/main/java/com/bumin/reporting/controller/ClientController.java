package com.bumin.reporting.controller;

import com.bumin.reporting.model.customer.CustomerInfoWrapper;
import com.bumin.reporting.model.transaction.TransactionRequest;
import com.bumin.reporting.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping(value="/api/v3",produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientController {

    @Autowired
    private ITransactionService transactionService;

    @PostMapping("/client")
    @ResponseStatus(HttpStatus.OK)
    CustomerInfoWrapper getClient(@RequestBody TransactionRequest request){
        Objects.requireNonNull(request.getTransactionId());

        return transactionService.getClient(request.getTransactionId());
    }
}
