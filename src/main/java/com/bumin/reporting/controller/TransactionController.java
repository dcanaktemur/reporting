package com.bumin.reporting.controller;

import com.bumin.reporting.model.transaction.TransactionReportRequest;
import com.bumin.reporting.model.transaction.TransactionReportResponse;
import com.bumin.reporting.model.transaction.TransactionRequest;
import com.bumin.reporting.model.transaction.TransactionResponse;
import com.bumin.reporting.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping(value="/api/v3",produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionController {

    @Autowired
    private ITransactionService transactionService;

    @PostMapping("/transactions/report")
    @ResponseStatus(HttpStatus.OK)
    TransactionReportResponse getTransactionsReport(@RequestBody TransactionReportRequest request) {
        Objects.requireNonNull(request.getFromDate());
        Objects.requireNonNull(request.getToDate());


        return transactionService.getTransactionReport(
                request.getFromDate(),
                request.getToDate(),
                request.getMerchant(),
                request.getAcquirer());
    }

    @PostMapping("/transaction/list")
    @ResponseStatus(HttpStatus.OK)
    void getTransactionList(){
        throw new UnsupportedOperationException();
    }

    @PostMapping("/transaction")
    @ResponseStatus(HttpStatus.OK)
    TransactionResponse getTransaction(@RequestBody TransactionRequest request){
        Objects.requireNonNull(request.getTransactionId());

        return transactionService.getTransaction(request.getTransactionId());

    }

}
