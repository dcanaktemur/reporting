package com.bumin.reporting.repository;

import com.bumin.reporting.model.db.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction,Long> {

    Transaction findByTransactionId(String transactionId);

    @Query(value = "SELECT * FROM TRANSACTION t " +
            "WHERE t.created_at between :fromDate AND :toDate " +
            "AND (:merchantId = 0 or t.merchant_id = :merchantId) " +
            "AND (:acquirerId = 0 or t.acquirer_transaction_id = :acquirerId)",nativeQuery = true)
    List<Transaction> findByCreatedAtBetweenAndMerchant_IdAndAcquirer_Id(
            @Param("fromDate") Date fromDate , @Param("toDate") Date toDate, @Param("merchantId") long merchantId, @Param("acquirerId") long acquirerId);
}
