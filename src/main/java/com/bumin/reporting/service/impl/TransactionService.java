package com.bumin.reporting.service.impl;

import com.bumin.reporting.model.Status;
import com.bumin.reporting.model.customer.CustomerInfo;
import com.bumin.reporting.model.customer.CustomerInfoWrapper;
import com.bumin.reporting.model.db.Transaction;
import com.bumin.reporting.model.transaction.*;
import com.bumin.reporting.repository.TransactionRepository;
import com.bumin.reporting.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TransactionService implements ITransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public TransactionResponse getTransaction(String transactionId) {
        Transaction transaction = transactionRepository.findByTransactionId(transactionId);

        TransactionResponse transactionResponse = null;
        if(transaction != null) {
            transactionResponse = createTransactionResponse(transaction);
        }

        return transactionResponse;
    }

    @Override
    public CustomerInfoWrapper getClient(String transactionId) {
        Transaction transaction = transactionRepository.findByTransactionId(transactionId);

        CustomerInfo customerInfo = null;
        if(transaction != null){
            customerInfo = transaction.getCustomerInfo();
        }

        return new CustomerInfoWrapper(customerInfo);
    }

    @Override
    public TransactionReportResponse getTransactionReport(Date fromDate, Date toDate, int merchant, int acquirer) {
        List<Transaction> transactions = transactionRepository.
                findByCreatedAtBetweenAndMerchant_IdAndAcquirer_Id(fromDate,toDate,merchant,acquirer);

        TransactionReportResponse transactionReportResponse = null;
        if(transactions != null && !transactions.isEmpty()){
           Map<String,List<Transaction>> transactionsPerCurrency = transactions.stream().collect(Collectors.groupingBy(o -> o.getFxMerchant().getOriginalCurrency()));

           transactionReportResponse = new TransactionReportResponse();
           transactionReportResponse.setStatus(Status.APPROVED.toString());
           List<TransactionReport> reports = new ArrayList<>();
           transactionReportResponse.setResponse(reports);
           transactionsPerCurrency.forEach((currency,transaction) -> {
                TransactionReport transactionReport = new TransactionReport();
                transactionReport.setCurrency(currency);
                transactionReport.setCount(transaction.size());
                double total = transaction.stream().
                        mapToDouble(d -> d.getFxMerchant().getOriginalAmount())
                        .sum();
                transactionReport.setTotal(total);
                reports.add(transactionReport);
           });
        }

        return transactionReportResponse;
    }

    private TransactionResponse createTransactionResponse(Transaction transaction) {
        TransactionResponse transactionResponse = new TransactionResponse();

        transactionResponse.setCustomerInfo(transaction.getCustomerInfo());

        Fx fx = new Fx();
        fx.setMerchant(transaction.getFxMerchant());
        transactionResponse.setFx(fx);

        transactionResponse.setMerchant(transaction.getMerchant());
        transactionResponse.setAcquirer(transaction.getAcquirer());

        TransactionWrapper transactionWrapper = new TransactionWrapper();
        transactionWrapper.setMerchant(transaction);
        transactionResponse.setTransaction(transactionWrapper);
        return transactionResponse;
    }


}
