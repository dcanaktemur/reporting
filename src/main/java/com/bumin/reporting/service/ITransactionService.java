package com.bumin.reporting.service;

import com.bumin.reporting.model.customer.CustomerInfoWrapper;
import com.bumin.reporting.model.transaction.TransactionReportResponse;
import com.bumin.reporting.model.transaction.TransactionResponse;

import java.util.Date;

public interface ITransactionService {
    TransactionResponse getTransaction(String transactionId);

    CustomerInfoWrapper getClient(String transactionId);

    TransactionReportResponse getTransactionReport(Date fromDate, Date toDate, int merchant, int acquirer);
}
