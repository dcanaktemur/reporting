package com.bumin.reporting.repository;

import com.bumin.reporting.model.user.User;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByEmail_thenReturnUser(){
        //given
        //You can look at data.sql
        String email = "demo@bumin.com.tr";

        //when
        User found = userRepository.findByEmail(email);

        //then
        Assertions.assertThat(found.getEmail()).isEqualTo(email);
    }


}
