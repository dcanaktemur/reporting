package com.bumin.reporting.repository;

import com.bumin.reporting.model.db.Transaction;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransactionRepositoryTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void whenFindByTransactionId_thenReturnTransaction(){

        //given
        // You can look at data.sql
        String transactionId = "1-1444392550-1";

        //when
        Transaction found = transactionRepository.findByTransactionId(transactionId);

        //then
        Assertions.assertThat(found.getTransactionId()).isEqualTo(transactionId);
    }

    @Test
    public void whenFindByCreatedAtBetweenAndMerchant_IdAndAcquirer_Id_thenReturnTransactions() throws ParseException {

        //given
        // You can look at data.sql
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date fromDate = formatter.parse("21-05-2018");
        Date toDate = formatter.parse("01-06-2018");
        //when
        List<Transaction> found = transactionRepository.findByCreatedAtBetweenAndMerchant_IdAndAcquirer_Id(fromDate,toDate,0,0);

        //then
        Assertions.assertThat(found).isNotEmpty();
    }

}
