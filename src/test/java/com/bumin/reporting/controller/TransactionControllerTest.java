package com.bumin.reporting.controller;

import com.bumin.reporting.model.Status;
import com.bumin.reporting.model.db.Transaction;
import com.bumin.reporting.model.transaction.*;
import com.bumin.reporting.model.user.User;
import com.bumin.reporting.security.JwtTokenUtil;
import com.bumin.reporting.security.JwtUserDetailsService;
import com.bumin.reporting.service.impl.TransactionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionControllerTest {

    private MockMvc mvc;

    @MockBean
    private TransactionService service;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser
    public void givenTransactionId_whenGetTransaction_thenReturnJsonObject() throws Exception {
        TransactionResponse transactionResponse = new TransactionResponse();
        TransactionWrapper transactionWrapper = new TransactionWrapper();
        Transaction transaction = new Transaction();
        transaction.setTransactionId("1-153546545-1");
        transactionResponse.setTransaction(transactionWrapper);
        transactionWrapper.setMerchant(transaction);

        given(service.getTransaction("1-153546545-1")).willReturn(transactionResponse);

        User user = new User("email","password");

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(),Collections.emptyList());

        when(jwtTokenUtil.getUsernameFromToken(any())).thenReturn(user.getEmail());
        when(jwtUserDetailsService.loadUserByUsername(eq(user.getEmail()))).thenReturn(userDetails);

        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTransactionId("1-153546545-1");

        byte[] transactionJson = toJson(transactionRequest);

        mvc.perform(post("/api/v3/transaction").
                header("Authorization","test.test.test")
                .contentType(MediaType.APPLICATION_JSON)
                .content(transactionJson)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
        .andExpect(jsonPath("$.transaction.merchant.transactionId", Is.is("1-153546545-1")));
    }

    @Test
    @WithMockUser
    public void givenReportRequest_whenGetTransactionReport_thenReturnJsonArray() throws Exception {
        TransactionReportResponse transactionReportResponse = new TransactionReportResponse();
        transactionReportResponse.setStatus(Status.APPROVED.toString());
        TransactionReport transactionReport = new TransactionReport();
        transactionReport.setCurrency("USD");
        transactionReport.setTotal(12346);
        transactionReport.setCount(123);
        List<TransactionReport> transactionReports = Arrays.asList(transactionReport);
        transactionReportResponse.setResponse(transactionReports);

        given(service.getTransactionReport(any(),any(),anyInt(),anyInt())).willReturn(transactionReportResponse);

        User user = new User("email","password");

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(),Collections.emptyList());

        when(jwtTokenUtil.getUsernameFromToken(any())).thenReturn(user.getEmail());
        when(jwtUserDetailsService.loadUserByUsername(eq(user.getEmail()))).thenReturn(userDetails);

        Date date = new Date();
        TransactionReportRequest transactionRequest = new TransactionReportRequest();
        transactionRequest.setFromDate(date);
        transactionRequest.setToDate(date);

        byte[] transactionJson = toJson(transactionRequest);

        mvc.perform(post("/api/v3/transactions/report").
                header("Authorization","test.test.test")
                .contentType(MediaType.APPLICATION_JSON)
                .content(transactionJson)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$.status",Is.is(Status.APPROVED.toString())))
                .andExpect(jsonPath("$.response[0].total", Is.is((double)12346)));
    }


    private byte[] toJson(Object r) throws Exception {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(r).getBytes();
    }

}
