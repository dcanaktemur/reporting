package com.bumin.reporting.controller;

import com.bumin.reporting.model.customer.CustomerInfo;
import com.bumin.reporting.model.customer.CustomerInfoWrapper;
import com.bumin.reporting.model.transaction.TransactionRequest;
import com.bumin.reporting.model.user.User;
import com.bumin.reporting.security.JwtTokenUtil;
import com.bumin.reporting.security.JwtUserDetailsService;
import com.bumin.reporting.service.impl.TransactionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.eq;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientControllerTest {

    private MockMvc mvc;

    @MockBean
    private TransactionService service;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser
    public void givenTransactionId_whenGetClient_thenReturnJsonObject() throws Exception {
        CustomerInfo customerInfo = new CustomerInfo();
        customerInfo.setEmail("demo@bumin.com.tr");
        CustomerInfoWrapper customerInfoWrapper = new CustomerInfoWrapper(customerInfo);

        given(service.getClient("1-1454645-1")).willReturn(customerInfoWrapper);

        User user = new User("email","password");

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(),Collections.emptyList());

        when(jwtTokenUtil.getUsernameFromToken(any())).thenReturn(user.getEmail());
        when(jwtUserDetailsService.loadUserByUsername(eq(user.getEmail()))).thenReturn(userDetails);

        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setTransactionId("1-1454645-1");

        byte[] transactionJson = toJson(transactionRequest);

        mvc.perform(post("/api/v3/client").
                header("Authorization","test.test.test")
                .contentType(MediaType.APPLICATION_JSON)
                .content(transactionJson)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
        .andExpect(jsonPath("$.customerInfo.email", Is.is("demo@bumin.com.tr")));
    }


    private byte[] toJson(Object r) throws Exception {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(r).getBytes();
    }

}
