package com.bumin.reporting.service;

import com.bumin.reporting.model.customer.CustomerInfo;
import com.bumin.reporting.model.customer.CustomerInfoWrapper;
import com.bumin.reporting.model.db.Transaction;
import com.bumin.reporting.model.transaction.FxMerchant;
import com.bumin.reporting.model.transaction.TransactionReportResponse;
import com.bumin.reporting.model.transaction.TransactionResponse;
import com.bumin.reporting.repository.TransactionRepository;
import com.bumin.reporting.service.impl.TransactionService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class TransactionServiceTest {

    @TestConfiguration
    static class TransactionServiceTestContextConfiguration {

        @Bean
        public TransactionService transactionService() {
            return new TransactionService();
        }
    }

    @Autowired
    private TransactionService transactionService;

    @MockBean
    private TransactionRepository transactionRepository;

    @Before
    public void setUp() {
        Transaction transaction = new Transaction();
        transaction.setTransactionId("1-1143454564-1");

        CustomerInfo customerInfo = new CustomerInfo();
        customerInfo.setEmail("demo@bumin.com.tr");
        transaction.setCustomerInfo(customerInfo);

        when(transactionRepository.findByTransactionId(transaction.getTransactionId()))
                .thenReturn(transaction);

        FxMerchant fxMerchant = new FxMerchant();
        fxMerchant.setOriginalAmount(100);
        fxMerchant.setOriginalCurrency("USD");
        transaction.setFxMerchant(fxMerchant);

        List<Transaction> transactions = Arrays.asList(transaction);

        when(transactionRepository.
                findByCreatedAtBetweenAndMerchant_IdAndAcquirer_Id(any(),any(),anyLong(),anyLong()))
                .thenReturn(transactions);
    }

    @Test
    public void whenGetTransaction_thenReturnTransaction(){
        String transactionId = "1-1143454564-1";

        TransactionResponse transaction = transactionService.getTransaction(transactionId);

        Assertions.assertThat(transaction.getTransaction().getMerchant().getTransactionId()).isEqualTo(transactionId);
    }

    @Test
    public void whenGetClient_thenReturnCustomerInfo(){
        String transactionId = "1-1143454564-1";

        CustomerInfoWrapper customerInfoWrapper = transactionService.getClient(transactionId);

        Assertions.assertThat(customerInfoWrapper.getCustomerInfo().getEmail()).isEqualTo("demo@bumin.com.tr");
    }

    @Test
    public void whenGetTransactionReport_thenReturnTransactionReportResponse(){
        Date date = new Date();
        TransactionReportResponse transactionReportResponse = transactionService.getTransactionReport(date,date,0,0);

        Assertions.assertThat(transactionReportResponse.getResponse().get(0).getCount()).
                isEqualTo(1);

        Assertions.assertThat(transactionReportResponse.getResponse().get(0).getCurrency()).
                isEqualTo("USD");

        Assertions.assertThat(transactionReportResponse.getResponse().get(0).getTotal()).
                isEqualTo(100);
    }
}
